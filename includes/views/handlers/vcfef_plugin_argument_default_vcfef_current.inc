<?php

/**
 * @file
 * Contains a default argument plugin to return the field values of current menu items.
 */

/**
 * Default argument plugin to get the current user's cart order ID.
 */
class vcfef_plugin_argument_default_vcfef_current extends views_plugin_argument_default {

  /**
   * Return the default argument.
   */
  public function get_argument() {
    switch ($this->options['entity_type']) {
      case 'autodetect':
        $entity_type = $this->detect_entity_type();
        break;

      default:
        $entity_type = $this->options['entity_type'];
        break;
    }

    // If the entity type can't be found, exit early.
    if (!$entity_type) {
      return 0;
    }

    // Collect info about the entity and field we're after.
    $entity_info = entity_get_info($entity_type);
    $field_name = $this->options['field_name'];

    // Figure out the url segment that the entity ID is located.
    $position = FALSE;
    $menu_item = menu_get_item();
    if (is_array($menu_item['load_functions'])) {
      foreach ($menu_item['load_functions'] as $key => $func_name) {
        if ($func_name == $entity_info['load hook']) {
          $position = $key;
          break;
        }
      }
    }

    // If we couldn't find a menu position to load the entity from, bail out.
    if ($position === FALSE) {
      return 0;
    }

    // Grab the entity object from the menu.
    $object = menu_get_object($entity_type, $position);

    // Determine language if this entity has a 'language' key available.
    $language = NULL;
    if (isset($entity_info['entity keys']['language'])) {
      if (!empty($object->{$entity_info['entity keys']['language']})) {
        $language = $object->{$entity_info['entity keys']['language']};
      }
    }

    // Get the field data from the entity.
    if (!empty($object)) {
      $items = field_get_items($entity_type, $object, $field_name, $language);
      if (!empty($items)) {
        $field_info = field_info_field($field_name);
        $column_key = key($field_info['columns']);
        return $items[0][$column_key];
      }
    }

    // Value of the field could not be found.
    return 0;
  }

  public function option_definition() {
    $options = parent::option_definition();
    $options['entity_type'] = array('default' => 'autodetect');
    $options['field_name'] = array('default' => '');

    return $options;
  }

  /**
   * Provide a options form for this handler.
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['entity_type'] = array(
      '#title' => t('Entity Type'),
      '#type' => 'select',
      '#options' => array('autodetect' => t('-- Autodetect entity type --')),
      '#default_value' => $this->options['entity_type'],
    );

    $form['field_name'] = array(
      '#title' => t('Field name'),
      '#type' => 'select',
      '#options' => array(),
      '#default_value' => $this->options['field_name'],
      '#id' => 'edit-field-name',
    );

    $form['field_help'] = array();

    $entities = entity_get_info();
    foreach ($entities as $entity_type => $info) {
      $form['entity_type']['#options'][$entity_type] = $info['label'];
      foreach ($info['bundles'] as $bundle => $bundle_info) {
        foreach (field_info_instances($entity_type, $bundle) as $field_name => $field_info) {
          $form['field_name']['#options'][$field_name] = $field_info['label'] . ' [' . $field_name . ']';
          $form['field_help'][$field_name]['#appears_in'][] = $entity_type . ':' . $bundle;
          $form['field_help'][$field_name]['#dependency']['edit-field-name'][] = $field_name;
        }
      }
    }
    asort($form['field_name']['#options']);

    foreach ($form['field_help'] as &$element) {
      $element['#type'] = 'fieldset';
      $vars['items'] = array();
      foreach ($element['#appears_in'] as $string) {
        list($entity, $bundle) = explode(':', $string, 2);
        $vars['items'][] = $entities[$entity]['label'] . ' - ' . $entities[$entity]['bundles'][$bundle]['label'];
      }
      $element['#title'] = 'Appears in';
      $element['#description'] = theme('item_list', $vars);
    }
  }

  /**
   * Function to detect the entity type for the current page.
   *
   * @return bool|string
   *   FALSE: Entity type could not be detected.
   *   string: Machine name of entity type.
   */
  public function detect_entity_type() {

    $entity_url_map = array();
    foreach (entity_get_info() as $key => $entity) {
      if (!empty($entity['default path'])) {
        $path = $entity['default path'];
        $path = substr($path, 0, strpos($entity['default path'], '%') + 1);
        $entity_url_map[$path] = $key;
      }
    }
    $item = menu_get_item();
    foreach ($entity_url_map as $path => $entity_type) {
      if (strpos($item['path'], $path) === 0) {
        return $entity_type;
      }
    }

    return FALSE;
  }

}
