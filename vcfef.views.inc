<?php

/**
 * @file
 * Views integration for the contextual filters.
 */

/**
 * Implements hook_views_plugins().
 */
function vcfef_views_plugins() {
  return array(
    'argument default' => array(
      'vcfef_current' => array(
        'title' => t("Field value from current entity"),
        'handler' => 'vcfef_plugin_argument_default_vcfef_current',
      ),
    ),
  );
}
